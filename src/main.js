var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var size = 25
var groupSet = []
var mazeArray = create2DArray(size, size, 0, false)
for (var i = 0; i < mazeArray.length; i++) {
  for (var j = 0; j < mazeArray[i].length; j++) {
    if (mazeArray[i][j] > -1) {
      groupSet.push([mazeArray[i][j],
        [i, j]
      ])
    }
  }
}
var pickedSet
var neighbours
var pickedNeighbour
var cellsToMerge

var frames = 0
var rotation = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < mazeArray.length; i++) {
    for (var j = 0; j < mazeArray[i].length; j++) {
      if (rotation === 0) {
        fill(255 / (size * size) * mazeArray[i][j])
      } else if (rotation === 1) {
        fill(255 - (255 / size) * (mazeArray[i][j] % size))
      } else if (rotation === 2) {
        fill(255 - 255 / (size * size) * mazeArray[i][j])
      } else if (rotation === 3) {
        fill((255 / size) * (mazeArray[i][j] % size))
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size))
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    if (groupSet.length !== 1) {
      pickedSet = pickRandomSet(groupSet)
      neighbours = getNeighbours(pickedSet)
      pickedNeighbour = pickRandomSet(neighbours)
      cellsToMerge = getAllCellsToBeMerged(pickedNeighbour, groupSet)
      for (var i = 0; i < cellsToMerge.length; i++) {
        pickedSet.push(cellsToMerge[i])
        mazeArray[cellsToMerge[i][0]][cellsToMerge[i][1]] = pickedSet[0]
      }
      for (var i = 0; i < groupSet.length; i++) {
        if (groupSet[i][0] === pickedNeighbour[0]) {
          groupSet.splice(i, 1)
        }
      }
    } else {
      mazeArray = create2DArray(size, size, 0, false)
      groupSet = []
      for (var i = 0; i < mazeArray.length; i++) {
        for (var j = 0; j < mazeArray[i].length; j++) {
          if (mazeArray[i][j] > -1) {
            groupSet.push([mazeArray[i][j],
              [i, j]
            ])
          }
        }
      }
      if (rotation < 3) {
        rotation++
      } else {
        rotation = 0
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function pickRandomSet(set) {
  var randomPick = set[Math.floor(Math.random() * set.length)]
  return randomPick
}

function getNeighbours(set) {
  var neighbours = []
  for (var i = 1; i < set.length; i++) {
    var posX = set[i][0]
    var posY = set[i][1]
    if (posX > 0) {
      if (isArrayInArray(set, [posX - 1, posY]) === false) {
        neighbours.push([mazeArray[posX - 1][posY],
          [posX - 1, posY]
        ])
      }
    }
    if (posX < size - 1) {
      if (isArrayInArray(set, [posX + 1, posY]) === false) {
        neighbours.push([mazeArray[posX + 1][posY],
          [posX + 1, posY]
        ])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(set, [posX, posY - 1]) === false) {
        neighbours.push([mazeArray[posX][posY - 1],
          [posX, posY - 1]
        ])
      }
    }
    if (posY < size - 1) {
      if (isArrayInArray(set, [posX, posY + 1]) === false) {
        neighbours.push([mazeArray[posX][posY + 1],
          [posX, posY + 1]
        ])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function getAllCellsToBeMerged(pick, set) {
  var cells = []
  for (var i = 0; i < set.length; i++) {
    if (set[i][0] === pick[0]) {
      for (var j = 1; j < set[i].length; j++) {
        cells.push(set[i][j])
      }
    }
  }
  return cells
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
